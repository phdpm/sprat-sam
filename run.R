#########################################################
### Perform SAM stock assessment               ##########
### With FLR and FLSAM overlay                 ##########
### Based on FLCore input data                 ##########
### Piatinskii M, Azov-Black sea branch, VNIRO ##########
#########################################################

library("FLCore")
library("FLAssess")
library("ggplotFL")
library("FLBRP")
library("FLash")
library("FLSAM")
library("dplyr")

# stock object configurations
config.stock.name <- "Sprat Black sea northeast" # fish title
config.stock.desc <- "Sprat stock information in FLCore format from 1994year in Russian waters (northeast of Black sea)" # fish stock description
config.stock.fbar.min <- 1 # minimum fish age become fishing (Fbar min)
config.stock.fbar.max <- 3 # maximum fish age at fishing (Fbar max)
config.stock.no_harvestspawn <- TRUE # Set TRUE if there is no information about fishing before spawning
config.stock.no_mspawn <- TRUE # Set TRUE if no information about natural mortality before spawning
config.stock.no_discards <- TRUE # Set TRUE if no separated information about landing/discards exist (only catch)

# configure surveys indices 
config.index1.name <- "Sprat crimea cpue index" 
config.index1.desc <- "Fishery index information about sprat abundance at Crimea shelf from real fishing surveys"
config.index1.startf <- 0.25 # start month of survey in format month/12, example: 1 feb = 2/12 = 0.166
config.index1.endf <- 0.83 # end month of survey in format month/12
config.index1.type <- "number" # type of survey, allowed: number, biomass

# same with previous
config.index2.name <- "Sprat caucasian cpue index" 
config.index2.desc <- "Fishery index information about sprat abundance at Caucasian shelf from real fishing surveys"
config.index2.startf <- 0.25
config.index2.endf <- 0.83
config.index2.type <- "number"

# read input data - stock
tmp.stock <- read.csv("input/stock.csv", sep=";")
data.stock <- as.FLStock(tmp.stock)
# read input data - index
tmp.idx1.idx <- read.csv("input/survey1/index.csv", sep=";")
tmp.idx1.eff <- read.csv("input/survey1/effort.csv", sep=";")
tmp.idx2.idx <- read.csv("input/survey2/index.csv", sep=";")
tmp.idx2.eff <- read.csv("input/survey2/effort.csv", sep=";")

data.index1 <- FLIndex(index=as.FLQuant(tmp.idx1.idx), effort=as.FLQuant(tmp.idx1.eff), catch.n=as.FLQuant(tmp.idx1.idx))
data.index2 <- FLIndex(index=as.FLQuant(tmp.idx2.idx), effort=as.FLQuant(tmp.idx2.eff), catch.n=as.FLQuant(tmp.idx2.idx))

remove(tmp.stock, tmp.idx1.idx, tmp.idx1.eff, tmp.idx2.idx, tmp.idx2.eff)
print("[SAM toolkit]: Load data: OK")
# Set name,desc from cfgs
name(data.stock) <- config.stock.name
desc(data.stock) <- config.stock.desc
# set fbar ranges
range(data.stock)[c("minfbar", "maxfbar")] <- c(config.stock.fbar.min, config.stock.fbar.max)

# fix closest to 0 values from XSA framework and set 0.01 = NA in surveys and stock.n
for (col in 1:length(data.index1@index[,1])) {
  for (row in 1:length(data.index1@index[1,])) {
    if (!is.na(data.index1@index[col,row]) & data.index1@index[col,row] < 0.2) data.index1@index[col,row] <- NA
  }
}

for (col in 1:length(data.index2@index[,1])) {
  for (row in 1:length(data.index2@index[1,])) {
    if (!is.na(data.index2@index[col,row]) & data.index2@index[col,row] < 0.2) data.index2@index[col,row] <- NA
  }
}

for (col in 1:length(data.stock@catch.n[,1])) {
  for (row in 1:length(data.stock@catch.n[1,])) {
    print(sprintf("col->row: %s;%s", col, row))
    if (!is.na(data.stock@catch.n[col,row]) & data.stock@catch.n[col,row] < 1) data.stock@catch.n[col,row] <- NA
  }
}


# set harvest.spwn and m.spwn = 0 if no data exist (or XSA throw error)
if (config.stock.no_harvestspawn) {
  harvest.spwn(data.stock) <- 0
}
if (config.stock.no_mspawn) {
  m.spwn(data.stock) <- 0
}
# if no info about discards/landings separatly than set discards = 0, landings = catch
if (config.stock.no_discards) {
  discards(data.stock) <- 0
  discards.n(data.stock) <- 0
  discards.wt(data.stock) <- catch.wt(data.stock)
  landings(data.stock) <- catch(data.stock)
  landings.n(data.stock) <- catch.n(data.stock)
  landings.wt(data.stock) <- catch.wt(data.stock)
}
stock.wt(data.stock) <- catch.wt(data.stock)

# set cfgs for index1 and index2
type(data.index1) <- config.index1.type
name(data.index1) <- config.index1.name
desc(data.index1) <- config.index1.desc
range(data.index1)[c("startf", "endf")] <- c(config.index1.startf, config.index1.endf)

type(data.index2) <- config.index2.type
name(data.index2) <- config.index2.name
desc(data.index2) <- config.index2.desc
range(data.index2)[c("startf", "endf")] <- c(config.index2.startf, config.index2.endf)
print("[SAM toolkit]: FLStock and FLIndex object configured successful")

# try to set 4 age as plusgroup
range(data.stock)[c("min", "max")] <- c(0,4)
data.stock <- setPlusGroup(data.stock, 4)

# build indices obj from idx1&idx2
data.indices <- FLIndices(data.index1, data.index2)

# build SAM control object
result.sam.control <- FLSAM.control(data.stock, data.indices)
result.sam.control@residuals <- TRUE

# try rough SAM model
result.sam.fit <- FLSAM(data.stock, data.indices, result.sam.control, return.fit=T)

result.sam.obj <- SAM2FLR(result.sam.fit, result.sam.control)
